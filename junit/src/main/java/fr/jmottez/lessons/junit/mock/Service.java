package fr.jmottez.lessons.junit.mock;

public class Service {

	private Dao dao;
	
	public Service(Dao dao){
		this.dao = dao;
	}
	
	public void save(Data data) {
		if(data.getValue() != null){
			dao.save(data);
		}
	}
	
	public Data findByValue(String value){
		Data data = dao.findByValue(value);
		data.setValue(data.getValue() + "truc");
		return data;
	}
	
}
