package fr.jmottez.lessons.junit.mock;

public class Data {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
