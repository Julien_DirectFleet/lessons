package fr.jmottez.lessons.designpattern.proxy;

public interface Image {
    void display();
}