package fr.jmottez.lessons.designpattern.iterator;


import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;

public class UserIterator implements Iterable<User> {

    private List<User> users = Lists.newArrayList();

    public Iterator<User> iterator() {
        return users.iterator();
    }

    public void add(User user) {
        users.add(user);
    }
}
