package fr.jmottez.lessons.designpattern.iterator;


public class IteratorMain {

    public static void main(String[] args) {

        UserIterator iterator = new UserIterator();

        User user0 = new User();
        user0.setName("name0");
        iterator.add(user0);

        User user1 = new User();
        user1.setName("name1");
        iterator.add(user1);

        User user2 = new User();
        user2.setName("name2");
        iterator.add(user2);


        for (User user : iterator) {
            System.out.println(user.getName());
        }
    }
}
