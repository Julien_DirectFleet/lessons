package fr.jmottez.lessons.designpattern.prototype;


public class PropotypeMain {

    public static void main(String[] args) throws CloneNotSupportedException {

        User user = new User();
        user.setName("name0");

        Vehicle vehicle0 = new Vehicle();
        vehicle0.setRef("ref0");

        user.getVehicles().add(vehicle0);

        User userCopy = user.clone();
        System.out.println(userCopy.getName());
        for(Vehicle vehicleCopy : userCopy.getVehicles()){
            System.out.println(vehicleCopy.getRef());
        }

    }


}
