package fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.entity;


public class VehicleJpaEntity {

    private String vhlIdentify;


    public String getVhlIdentify() {
        return vhlIdentify;
    }

    public void setVhlIdentify(String vhlIdentify) {
        this.vhlIdentify = vhlIdentify;
    }
}

