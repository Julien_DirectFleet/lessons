package fr.jmottez.lessons.designpattern.abs.factory.user.repository.jdbc;


import fr.jmottez.lessons.designpattern.abs.factory.user.model.UserModel;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import com.google.common.collect.Lists;

import java.util.List;

public class JdbcUserDao implements UserDao {

    private List<UserModel> users = Lists.newArrayList();

    public JdbcUserDao() {
        UserModel user0 = new UserModel();
        user0.setName("jdbcUser0");
        users.add(user0);
        UserModel user1 = new UserModel();
        user1.setName("jdbcUser1");
        users.add(user1);
    }

    public List<UserModel> findAll() {
        return users;
    }

    public List<UserModel> findByName(String name) {
        List<UserModel> selected = Lists.newArrayList();
        for (UserModel eachUser : users) {
            if (eachUser.getName().equals(name)) {
                selected.add(eachUser);
            }
        }
        return selected;
    }
}
