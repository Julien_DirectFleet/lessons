package fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.assembler;


import fr.jmottez.lessons.designpattern.abs.factory.vehicle.model.VehicleModel;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.entity.VehicleJpaEntity;
import com.google.common.collect.Lists;

import java.util.List;

public class VehicleJpaEntityAssembler {

    public List<VehicleModel> fromEntities(List<VehicleJpaEntity> entities) {
        List<VehicleModel> models = Lists.newArrayList();
        for (VehicleJpaEntity entity : entities) {
            models.add(fromEntity(entity));
        }
        return models;
    }

    private VehicleModel fromEntity(VehicleJpaEntity entity) {
        VehicleModel model = new VehicleModel();
        model.setRegistrationNumber(entity.getVhlIdentify());
        return model;
    }
}
