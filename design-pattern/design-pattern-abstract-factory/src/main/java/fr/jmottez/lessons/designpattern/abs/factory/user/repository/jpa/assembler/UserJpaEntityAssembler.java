package fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.assembler;

import fr.jmottez.lessons.designpattern.abs.factory.user.model.UserModel;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.entity.UserJpaEntity;
import com.google.common.collect.Lists;

import java.util.List;

public class UserJpaEntityAssembler {

    public List<UserModel> fromEntities(List<UserJpaEntity> entities) {
        List<UserModel> models = Lists.newArrayList();
        for (UserJpaEntity entity : entities) {
            models.add(fromEntity(entity));
        }
        return models;
    }

    private UserModel fromEntity(UserJpaEntity entity) {
        UserModel model = new UserModel();
        model.setName(entity.getUsrName());
        return model;
    }

}
