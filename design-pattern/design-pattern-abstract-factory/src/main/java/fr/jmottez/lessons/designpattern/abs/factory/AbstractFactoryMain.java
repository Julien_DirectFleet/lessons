package fr.jmottez.lessons.designpattern.abs.factory;


import fr.jmottez.lessons.designpattern.abs.factory.dao.DaoFactory;
import fr.jmottez.lessons.designpattern.abs.factory.dao.jdbc.JdbcDaoFactory;
import fr.jmottez.lessons.designpattern.abs.factory.dao.jpa.JpaDaoFactory;
import fr.jmottez.lessons.designpattern.abs.factory.user.model.UserModel;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.model.VehicleModel;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;

import java.util.List;

public class AbstractFactoryMain {

    public static void main(String[] args) {
        DaoFactory daoFactory0 = new JdbcDaoFactory();
        exec(daoFactory0, "jdbcVehicle1");

        DaoFactory daoFactory1 = new JpaDaoFactory();
        exec(daoFactory1, "jpaVhl1");
    }

    private static void exec(DaoFactory daoFactory, String parameter) {
        UserDao userDao = daoFactory.userDao();
        List<UserModel> users = userDao.findAll();
        for (UserModel user : users) {
            System.out.println("User:" + user.getName());
        }

        VehicleDao vehicleDao = daoFactory.vehicleDao();
        List<VehicleModel> vehicles = vehicleDao.findByRegistrationNumber(parameter);
        for (VehicleModel vehicle : vehicles) {
            System.out.println("Vehicle:" + vehicle.getRegistrationNumber());
        }
    }

}
