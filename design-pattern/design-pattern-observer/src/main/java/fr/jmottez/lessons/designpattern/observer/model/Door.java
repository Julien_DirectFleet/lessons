package fr.jmottez.lessons.designpattern.observer.model;


import fr.jmottez.lessons.designpattern.observer.event.AlertEvent;
import fr.jmottez.lessons.designpattern.observer.observer.Observed;

public class Door extends Observed {

    public void onBreaking() {
        notify(new AlertEvent());
    }
}
