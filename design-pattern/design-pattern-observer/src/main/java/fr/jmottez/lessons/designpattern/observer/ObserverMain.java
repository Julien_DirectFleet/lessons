package fr.jmottez.lessons.designpattern.observer;


import fr.jmottez.lessons.designpattern.observer.model.Camera;
import fr.jmottez.lessons.designpattern.observer.model.Door;
import fr.jmottez.lessons.designpattern.observer.model.House;

public class ObserverMain {


    public static void main(String[] args) {

        Camera cam0 = new Camera();
        Camera cam1 = new Camera();
        Door door = new Door();
        House house = new House();

        cam0.add(house);
        cam1.add(house);
        door.add(house);

        cam0.seeSomeOne();

        door.onBreaking();


    }




}
