package fr.jmottez.lessons.designpattern.observer.observer;


import fr.jmottez.lessons.designpattern.observer.event.Event;

public interface Observer {

    void onEvent(Event event);

}
