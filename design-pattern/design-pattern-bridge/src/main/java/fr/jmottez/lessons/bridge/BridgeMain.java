package fr.jmottez.lessons.bridge;


public class BridgeMain {


    public static void main(String[] args) {

        String file = "toto";

        Printer printer = new PaperPrinter();
        printer.print(file);

        Printer otherPrinter = new PdfPrinter();
        otherPrinter.print(file);




    }


}
