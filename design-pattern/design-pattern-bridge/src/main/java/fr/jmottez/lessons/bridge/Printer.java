package fr.jmottez.lessons.bridge;


public interface Printer {

    void print(String file);
}
