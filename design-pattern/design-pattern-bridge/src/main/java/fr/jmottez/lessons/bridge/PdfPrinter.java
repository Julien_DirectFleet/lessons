package fr.jmottez.lessons.bridge;


public class PdfPrinter implements Printer {

    public void print(String file) {
        System.out.println("PDF Printer :" + file);
    }

}
