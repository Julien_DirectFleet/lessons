package fr.jmottez.lessons.designpattern.decorator.model;


public class Gaufre extends Dessert {
    public Gaufre() {
        setLabel("Gaufre");
        setPrice(1.80);
    }
}
