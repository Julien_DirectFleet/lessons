package fr.jmottez.lessons.designpattern.decorator.decorator;


import fr.jmottez.lessons.designpattern.decorator.model.Dessert;

public class WithChantilly extends DessertDecorator {

    public WithChantilly(Dessert dessert) {
        super(dessert);
    }

    public String getLabel() {
        return getDessert().getLabel() + ", chantilly";
    }

    public double getPrice() {
        return getDessert().getPrice() + 0.50;
    }
}
