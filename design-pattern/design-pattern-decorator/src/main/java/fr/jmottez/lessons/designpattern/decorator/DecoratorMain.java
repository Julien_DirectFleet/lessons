package fr.jmottez.lessons.designpattern.decorator;

import fr.jmottez.lessons.designpattern.decorator.decorator.WithChantilly;
import fr.jmottez.lessons.designpattern.decorator.decorator.WithChocolat;
import fr.jmottez.lessons.designpattern.decorator.model.Crepe;
import fr.jmottez.lessons.designpattern.decorator.model.Dessert;
import fr.jmottez.lessons.designpattern.decorator.model.Gaufre;

public class DecoratorMain {


    public static void main(String[] args) {

        Dessert dessert0 = new Gaufre();
        dessert0 = new WithChocolat(dessert0);
        System.out.println(dessert0);

        Dessert dessert1 = new Crepe();
        dessert1 = new WithChantilly(dessert1);
        dessert1 = new WithChocolat(dessert1);
        System.out.println(dessert1);
    }


}
