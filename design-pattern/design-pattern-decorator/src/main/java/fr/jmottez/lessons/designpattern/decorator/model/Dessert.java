package fr.jmottez.lessons.designpattern.decorator.model;


public class Dessert {

    private String label;

    private double price;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return getLabel() + " : " + getPrice();
    }

}
