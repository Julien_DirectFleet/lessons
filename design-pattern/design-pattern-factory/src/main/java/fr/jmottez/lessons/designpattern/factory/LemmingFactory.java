package fr.jmottez.lessons.designpattern.factory;

import fr.jmottez.lessons.designpattern.factory.model.lemming.*;

public class LemmingFactory {

    public Lemming create(LemmingType type) {
        switch (type) {
            case CLIMBER:
                return new ClimberLemming();
            case BOMBER:
                return new BomberLemming();
            case DIGGER:
                return new DiggerLemming();
            default:
                throw new IllegalArgumentException();
        }
    }


}
