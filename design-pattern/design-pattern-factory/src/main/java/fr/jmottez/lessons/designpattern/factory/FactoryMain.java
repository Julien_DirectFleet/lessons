package fr.jmottez.lessons.designpattern.factory;


import com.google.common.collect.Lists;
import fr.jmottez.lessons.designpattern.factory.model.lemming.Lemming;
import fr.jmottez.lessons.designpattern.factory.model.lemming.LemmingType;

import java.util.List;

public class FactoryMain {

    public static void main(String[] args) {
        LemmingFactory factory = new LemmingFactory();

        List<Lemming> list = Lists.newArrayList();
        list.add(factory.create(LemmingType.CLIMBER));
        list.add(factory.create(LemmingType.DIGGER));
        list.add(factory.create(LemmingType.BOMBER));

        for (Lemming lemming : list) {
            lemming.action();
        }
    }
}
