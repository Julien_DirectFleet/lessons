package fr.jmottez.lessons.designpattern.factory.model.lemming;

public class DiggerLemming implements Lemming {

    public void action() {
        System.out.println("Dig !!");
    }

}