package fr.jmottez.lessons.designpattern.lazy;


public class LazyLoadingMain {


    public static void main(String[] args) {
        System.out.println("start");
        LazyLoadingObject object = new LazyLoadingObject();
        System.out.println("Ma key est :" + object.getKey());
        System.out.println("Chargement ...");
        System.out.println("Ma valeur chargé:" + object.getValue());
        System.out.println("Ma valeur chargé:" + object.getValue());
        System.out.println("end");

    }


}
