package fr.jmottez.lessons.builder;


import fr.jmottez.lessons.builder.exception.MenuBuilderException;
import fr.jmottez.lessons.builder.model.FriesType;
import fr.jmottez.lessons.builder.model.FastFoodMenu;
import fr.jmottez.lessons.builder.model.MenuType;
import fr.jmottez.lessons.builder.model.burger.TheBigBurger;
import fr.jmottez.lessons.builder.model.burger.TheRoyalBurger;
import fr.jmottez.lessons.builder.model.dessert.Cookie;
import fr.jmottez.lessons.builder.model.dessert.Sunday;


public class BuilderMain {

    private static FastFoodMenuBuilder builder = new FastFoodMenuBuilder();

    public static void main(String[] args) {


        try {
            FastFoodMenu menu0 = builder.clean() //
                    .setMenuType(MenuType.STANDARD)
                    .setBurger(new TheBigBurger()) //
                    .setFriesType(FriesType.FRENCH_FRIES) //
                    .build(); //
            System.out.println(menu0);
        } catch (MenuBuilderException exception) {
            System.out.println(exception.getMessage());
        }

        try {
            FastFoodMenu menu1 = builder.clean() //
                    .setBurger(new TheRoyalBurger()) //
                    .addDessert(new Sunday()) //
                    .setFriesType(FriesType.POTATOES) //
                    .setMenuType(MenuType.MAXI)
                    .addDessert(new Cookie()) //
                    .build(); //
            System.out.println(menu1);
        } catch (MenuBuilderException exception) {
            System.out.println(exception.getMessage());
        }

        try {
            FastFoodMenu menu2 = builder.clean() //
                    .setBurger(new TheBigBurger()) //
                    .build(); //
            System.out.println(menu2);
        } catch (MenuBuilderException exception) {
            System.out.println(exception.getMessage());
        }


    }


}
