package fr.jmottez.lessons.builder;

import fr.jmottez.lessons.builder.model.FastFoodMenu;
import fr.jmottez.lessons.builder.model.MenuType;
import fr.jmottez.lessons.builder.model.burger.Burger;
import fr.jmottez.lessons.builder.model.burger.TheBigBurger;
import fr.jmottez.lessons.builder.model.burger.TheRoyalBurger;
import fr.jmottez.lessons.builder.model.dessert.Dessert;

import java.util.List;


public class FastFoodMenuPriceCalculator {

    private static final double REDUC_MENU_FIRST_DESSERT = 0.5;

    public double calculate(FastFoodMenu menu) {
        double price = priceType(menu.getType());
        price += priceBurger(menu.getBurger());
        price += priceMenuDessert(menu.getDesserts());
        return price;
    }

    private double priceType(MenuType type) {
        switch (type) {
            case MAXI:
                return 5.0;
            case STANDARD:
                return 3.0;
            default:
                return 0.0;
        }
    }

    private double priceBurger(Burger burger) {
        if (burger.getClass().equals(TheBigBurger.class)) {
            return 3.0;
        }
        if (burger.getClass().equals(TheRoyalBurger.class)) {
            return 2.0;
        }
        return 0.0;
    }

    private double priceMenuDessert(List<Dessert> desserts) {
        double priceDessert = priceDessert(desserts);
        if (priceDessert > 0) {
            priceDessert -= REDUC_MENU_FIRST_DESSERT;
        }
        return priceDessert;
    }

    private double priceDessert(List<Dessert> desserts) {
        return desserts.size() * 1.5;
    }

}
