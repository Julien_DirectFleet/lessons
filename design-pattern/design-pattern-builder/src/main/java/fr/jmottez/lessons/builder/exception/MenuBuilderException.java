package fr.jmottez.lessons.builder.exception;


public class MenuBuilderException extends Exception {
    public MenuBuilderException(String errorMessage) {
        super(errorMessage);
    }
}
