package fr.jmottez.lessons.adapter;


public class Adapter extends UserToAdapte  {


    private UserAdapted adapted;

    public Adapter(UserAdapted adapted) {
        this.adapted = adapted;
    }

    @Override
    public String getFullName() {
        return adapted.getFirstName() + " " + adapted.getLastName();
    }
}
