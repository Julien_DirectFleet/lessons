package fr.jmottez.lessons.designpattern.strategy;


import fr.jmottez.lessons.designpattern.strategy.payment.PaymentStrategy;
import com.google.common.collect.Lists;

import java.util.List;

public class ShoppingCart {

    private List<Item> items = Lists.newArrayList();


    public void addItem(Item item){
        this.items.add(item);
    }

    public int calculateTotal(){
        int sum = 0;
        for(Item item : items){
            sum += item.getPrice();
        }
        return sum;
    }

    public void pay(PaymentStrategy paymentMethod){
        int amount = calculateTotal();
        paymentMethod.pay(amount);
    }

}
