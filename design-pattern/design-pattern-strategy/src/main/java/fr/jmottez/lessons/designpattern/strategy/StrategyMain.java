package fr.jmottez.lessons.designpattern.strategy;


import fr.jmottez.lessons.designpattern.strategy.payment.CreditCardStrategy;
import fr.jmottez.lessons.designpattern.strategy.payment.PaypalStrategy;

public class StrategyMain {


    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();

        Item item1 = new Item("Fromages",10);
        Item item2 = new Item("Chaussettes",40);

        cart.addItem(item1);
        cart.addItem(item2);

        //pay by paypal
        cart.pay(new PaypalStrategy("myemail@example.com", "mypwd"));

        //pay by credit card
        cart.pay(new CreditCardStrategy("Toto", "1234567890123456", "786", "12/15"));

    }
}
