package fr.jmottez.lessons.designpattern.strategy.payment;


public class PaypalStrategy implements PaymentStrategy {

    private String email;
    private String password;

    public PaypalStrategy(String email, String pwd) {
        this.email = email;
        this.password = pwd;
    }

    public void pay(int amount) {
        int totalAmount = amount + 10;
        System.out.println("Paiement par Paypal de " + totalAmount + ":" + email);
    }

}

