package fr.jmottez.lessons.designpattern.strategy.payment;

public interface PaymentStrategy {

    void pay(int amount);

}
